import pathlib

from flask import Flask
from flask_restful import Api

from constants import UPLOAD_FOLDER
from logger import logger, logger_init


def create_app():
    app = Flask(__name__)
    api = Api(app)

    from resources import CorpusUpload, AnalysisResult
    api.add_resource(CorpusUpload, '/upload')
    api.add_resource(AnalysisResult, '/result')

    app_init()
    return app


def app_init():
    logger_init('app')
    logger.debug('Started chronopress-backend.')
    pathlib.Path(UPLOAD_FOLDER).mkdir(exist_ok=True)

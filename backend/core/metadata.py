import pandas as pd

from logger import logger
from utils.dataframe import df_datetime_to_str


class InvalidMetaError(Exception):
    pass


class MetaData:
    def __init__(self, data: pd.DataFrame):
        self._data = data
        if self._data.columns[0] == 'filename':
            self._data = self._data.set_index('filename')
            df_datetime_to_str(self._data)
        else:
            raise InvalidMetaError('passed dataframe has invalid schema')

    @property
    def data(self):
        return self._data

    @property
    def categories(self):
        return {
            column: self._data[column].unique()
            for column in self._data.columns
        }

    def __getitem__(self, filename):
        return self._data.loc[filename].to_dict()

    @classmethod
    def from_excel(cls, excel_path):
        logger.debug('Reading metadata from excel.')
        data = pd.read_excel(excel_path)
        logger.debug('Reading from excel finished.')
        return cls(data)


def load_metadata(filename):
    logger.debug('Loading metadata.')
    metadata = None
    extension = filename.split('.')[-2:]
    try:
        if extension[0] == 'meta' and extension[1] in ['xlsx']:
            if extension[1] == 'xlsx':
                metadata = MetaData.from_excel(filename)
    except (InvalidMetaError, FileNotFoundError) as e:
        logger.error(e)
    logger.debug('Metadata loaded.')
    return metadata

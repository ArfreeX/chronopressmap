import mongoengine

from logger import logger


from constants import (
    MONGO_DB_ALIAS,
    MONGO_DB_NAME,
    MONGO_DB_HOST,
    MONGO_DB_PORT,
)


def db_init():
    logger.debug('Initializing database.')
    mongoengine.register_connection(
        alias=MONGO_DB_ALIAS,
        name=MONGO_DB_NAME,
        host=MONGO_DB_HOST,
        port=MONGO_DB_PORT
    )
    logger.debug('Connection with DB registered.')

from mongoengine import (
    EmbeddedDocument,
    StringField,
    IntField,
    FloatField,
    BooleanField,
    EmbeddedDocumentField
)

from .geometry import Geometry, make_geometry
from geo_location.geo_code.geo_code_provider_interface import GeoCodeProviderInterface


class Location(EmbeddedDocument):
    name = StringField(required=True)
    count = IntField(default=1)
    center_lng = FloatField(required=True)
    center_lat = FloatField(required=True)
    is_center_preferred = BooleanField(required=True)
    geometry = EmbeddedDocumentField(Geometry)

    @staticmethod
    def from_geocode(geo_code: GeoCodeProviderInterface.GeoCode):
        return Location(
            name=geo_code.name,
            count=geo_code.count,
            center_lng=geo_code.geo_code_data.center_lng,
            center_lat=geo_code.geo_code_data.center_lat,
            is_center_preferred=geo_code.geo_code_data.is_center_preferred,
            geometry=make_geometry(geo_code.geo_code_data.geo_json)
        )

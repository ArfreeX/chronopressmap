import datetime

from mongoengine import (
    DateTimeField,
    Document,
    EmbeddedDocumentListField,
    StringField,
    DictField,
)


from constants import MONGO_DB_ALIAS
from utils.common import get_uuid
from .corpus_file import CorpusFile


class Result(Document):
    uuid = StringField(default=get_uuid)
    date = DateTimeField(default=datetime.datetime.now)
    corpus_name = StringField(required=True)
    corpus_files = EmbeddedDocumentListField(CorpusFile, required=True)
    all_categories = DictField(required=True)

    meta = {
        'db_alias': MONGO_DB_ALIAS,
        'collection': 'results',
        'indexes': [
            'uuid',
        ]
    }

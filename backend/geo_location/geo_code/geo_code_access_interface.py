from abc import abstractmethod, ABC
from dataclasses import dataclass
from typing import Dict, List, Iterable, Any


class GeoCodeAccessInterface(ABC):
    REQUESTS_TIMEOUT_SECONDS = 30

    @abstractmethod
    def get_geo_locations(self, names: Iterable[str]) -> 'GeoCodeAccessInterface.Response':
        pass

    @dataclass
    class GeoCodeData(object):
        result_name: str
        country_code: str
        importance: float
        center_lng: float
        center_lat: float
        geo_json: Dict[str, Any]
        is_center_preferred: bool

    class Response(object):
        def __init__(self):
            self.successful: bool = False
            self.geo_data: Dict[str, List[GeoCodeAccessInterface.GeoCodeData]] = {}

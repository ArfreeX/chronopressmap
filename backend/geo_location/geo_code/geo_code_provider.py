from functools import partial
from typing import Dict

import textdistance

from geo_location.geo_code.geo_code_access_interface import GeoCodeAccessInterface
from geo_location.geo_code.geo_code_provider_interface import GeoCodeProviderInterface, GeoCodeProviderException

from logger import logger


class GeoCodeProvider(GeoCodeProviderInterface):
    def __init__(self, geo_code_access: GeoCodeAccessInterface):
        self._geo_code_access = geo_code_access

    def get_geo_codes(self, names_with_count: Dict[str, int]) -> Dict[str, GeoCodeProviderInterface.GeoCode]:
        logger.debug('Getting geo codes.')
        geo_locations_resp = self._geo_code_access.get_geo_locations(names_with_count.keys())
        logger.debug('Geo locations response: {}'.format(geo_locations_resp))
        if not geo_locations_resp.successful:
            raise GeoCodeProviderException('Geo access was unsuccessful')
        resp = {}
        logger.debug('Selecting best result.')
        for name, geo_code_results in geo_locations_resp.geo_data.items():
            resp[name] = self._select_best_result(name, names_with_count[name], geo_code_results)
        logger.debug('Final geo codes: {}'.format(resp))
        return resp

    def _select_best_result(self, name, count, geo_code_results):
        """Dummy algorithm for selecting best result from results set"""
        if len(geo_code_results) == 0:
            return GeoCodeProviderInterface.GeoCode(name, 0, 0, False)
        geo_code_results.sort(key=partial(GeoCodeProvider._get_score, name), reverse=True)
        return GeoCodeProviderInterface.GeoCode(name, count, geo_code_results[0])

    @staticmethod
    def _get_score(name, geo_code):
        country_promotion = 3
        similarity = textdistance.ratcliff_obershelp(name, geo_code.result_name)
        return geo_code.importance + similarity + country_promotion if geo_code.country_code == 'pl' else 0

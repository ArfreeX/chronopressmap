from typing import Iterable
from warnings import warn

import requests

from constants import HTTP_SUCCESS_RESPONSE_CODE
from geo_location.geo_code.geo_code_access_interface import GeoCodeAccessInterface
from logger import logger


class GeoNamesAccess(GeoCodeAccessInterface):
    """GeoNamesAccess is deprecated due to missing implementation for complex locations geometries"""

    def __init__(self, max_rows=5):
        warn('GeoNamesAccess is deprecated, use Nominatim instead')
        self._max_rows = max_rows

    def get_geo_locations(self, names: Iterable[str]) -> GeoCodeAccessInterface.Response:
        logger.debug('Getting geo locations.')
        response = GeoCodeAccessInterface.Response()
        response.successful = True

        try:
            for name in names:
                req = self._build_req(name)
                status, partial_response = self._build_partial_response(req())
                if not status:
                    response.successful = False
                    return response

                response.geo_data[name] = partial_response

        except requests.exceptions.Timeout:
            response.successful = False

        logger.debug('Getting geo locations finished.')
        return response

    def _build_req(self, name):
        req_data = {'q': name,
                    'maxRows': self._max_rows,
                    'username': 'chronopressmap'
                    }

        def send_req_closure():
            return requests.post('http://api.geonames.org/searchJSON', data=req_data,
                                 timeout=GeoCodeAccessInterface.REQUESTS_TIMEOUT_SECONDS)

        return send_req_closure

    def _build_partial_response(self, raw_resp: requests.Response):
        partial_response = []
        successful = (raw_resp.status_code == HTTP_SUCCESS_RESPONSE_CODE)
        if not successful:
            return False, partial_response
        for geo_name in raw_resp.json()['geonames']:
            if 'countryCode' in geo_name:
                partial_response.append(GeoCodeAccessInterface.GeoCodeData(geo_name['toponymName'],
                                                                           geo_name['countryCode'].lower(),
                                                                           0,
                                                                           geo_name['lat'],
                                                                           geo_name['lng']))
        return True, partial_response

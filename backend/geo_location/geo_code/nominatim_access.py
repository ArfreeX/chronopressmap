from typing import Iterable

import requests
from requests.adapters import HTTPAdapter

from constants import HTTP_SUCCESS_RESPONSE_CODE
from geo_location.geo_code.geo_code_access_interface import GeoCodeAccessInterface
from logger import logger


class NominatimAccess(GeoCodeAccessInterface):
    NOMINATIM_URL = 'https://nominatim.openstreetmap.org'
    MAX_CONNECTION_RETIRES = 3
    HEADERS = {
        'User-Agent': 'Chronopressmap PWr - Flask Server 1.0',
        'From': 'chronopressmap@outlook.com'
    }

    def __init__(self, max_rows=5):
        self._max_rows = max_rows

    def get_geo_locations(self, names: Iterable[str]) -> GeoCodeAccessInterface.Response:
        logger.debug('Getting geo locations.')
        response = GeoCodeAccessInterface.Response()
        response.successful = True

        with requests.Session() as session:
            conn_adapter = HTTPAdapter(max_retries=NominatimAccess.MAX_CONNECTION_RETIRES)
            session.mount(NominatimAccess.NOMINATIM_URL, conn_adapter)
            try:
                for name in names:
                    req = self._build_req(name, session)
                    status, partial_response = self._build_partial_response(req())
                    if not status:
                        response.successful = False
                        return response
                    response.geo_data[name] = partial_response

            except requests.exceptions.RequestException:
                response.successful = False

        logger.debug('Getting geo locations finished.')
        return response

    def _build_req(self, name, session):
        req_data = {'q': name,
                    'limit': self._max_rows,
                    'addressdetails': '1',
                    'accept-language': 'PL',
                    'polygon_geojson': '1',
                    'polygon_threshold': '0.2',
                    'format': 'jsonv2'
                    }

        def send_req_closure():
            return session.get(NominatimAccess.NOMINATIM_URL + '/search', params=req_data,
                               headers=NominatimAccess.HEADERS,
                               timeout=GeoCodeAccessInterface.REQUESTS_TIMEOUT_SECONDS)

        return send_req_closure

    def _build_partial_response(self, raw_resp: requests.Response):
        partial_response = []
        successful = (raw_resp.status_code == HTTP_SUCCESS_RESPONSE_CODE)
        if not successful:
            return False, partial_response
        for geo_loc in raw_resp.json():
            try:
                geo_code_data = NominatimAccess._make_geo_code_data(geo_loc)
                if geo_code_data is not None:
                    partial_response.append(geo_code_data)
            except KeyError:
                pass

        return True, partial_response

    @staticmethod
    def _make_geo_code_data(geo_loc):
        if 'geojson' not in geo_loc:
            return None

        if 'address' not in geo_loc or 'country_code' not in geo_loc['address']:
            country_code = 'unknown'
        else:
            country_code = geo_loc['address']['country_code'].lower()

        return GeoCodeAccessInterface.GeoCodeData(
            result_name=geo_loc['display_name'],
            country_code=country_code,
            importance=geo_loc['importance'],
            center_lng=geo_loc['lon'],
            center_lat=geo_loc['lat'],
            geo_json=geo_loc['geojson'],
            is_center_preferred=NominatimAccess._is_center_point_preferred(geo_loc)
        )

    @staticmethod
    def _is_center_point_preferred(geo_loc):
        if geo_loc['geojson']['type'].lower() == 'point':
            return True

        if geo_loc['category'] not in ('boundary', 'place'):
            return True

        if geo_loc['category'] == 'place':
            return NominatimAccess._is_place_point_like(geo_loc['type'])

        return NominatimAccess._is_boundary_point_like(geo_loc)

    @staticmethod
    def _is_place_point_like(cat_type):
        return cat_type not in ('country', 'state', 'region', 'province', 'district', 'county', 'municipality')

    @staticmethod
    def _is_boundary_point_like(geo_loc):
        if geo_loc['type'] != 'administrative':
            return True

        if 'address' not in geo_loc:
            return True

        if any((True for key in geo_loc['address'] if key in ('city', 'town', 'village', 'hamlet'))):
            return True

        return False

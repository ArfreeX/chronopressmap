#!/bin/bash

venv_libs=$((echo "from distutils.sysconfig import get_python_lib" ; echo "print(get_python_lib())") | venv/bin/python)

mkdir -p /usr/local/lib/wrap_lem && \
	cp -r ./lib_wrap_lem/share/* /usr/local/share/ && \
	cp -r ./lib_wrap_lem/python/* $venv_libs/ && \
	cp -r ./lib_wrap_lem/lib/* /usr/local/lib/wrap_lem/


exit 0

import os
import time

import requests

import constants

from logger import logger

from utils import zip_utils


class NerAccessException(Exception):
    pass


class NerAccess(object):
    AVAILABLE_MODELS = {'5nam', 'top9', 'n82'}
    NLP_SERVICE_URL_BASE = 'http://ws.clarin-pl.eu/nlprest2/base'
    POLL_TIME_INTERVAL_SECONDS = 0.5
    REQUESTS_TIMEOUT_SECONDS = 20

    def __init__(self, ner_model):
        if ner_model not in NerAccess.AVAILABLE_MODELS:
            raise RuntimeError(f'Invalid ner model provided: {ner_model}, expected on of: {NerAccess.AVAILABLE_MODELS}')
        self.ner_model = ner_model

    def process_corpus(self, file_path) -> str:
        """
        Uses external NLP service to process zipped corpuses package

        :param file_path: path to zip file containing corpuses
        :return: path to stored processing result
        """

        logger.debug('Processing corpus: {}'.format(file_path))
        if not zip_utils.validate_zip_format(file_path):
            raise NerAccessException('Invalid input file, file is not zip archive')

        try:
            file_id = self._upload_corpus(file_path)
            task_id = self._start_nlp_task(file_id)
            ner_resp_id = self._poll_nlp_service_status(task_id)
        except requests.exceptions.RequestException as ex:
            raise NerAccessException(f'NLP service error: {ex}')

        logger.debug('Corpus {} processed.'.format(file_path))
        return self._store_ner_result(file_path, ner_resp_id)

    def _upload_corpus(self, file_path):
        logger.debug('Uploading corpus.')
        with open(file_path, "rb") as file_handle:
            doc = file_handle.read()

        resp = requests.post(NerAccess.NLP_SERVICE_URL_BASE + '/upload/', data=doc,
                             headers={'Content-Type': 'binary/octet-stream'},
                             timeout=NerAccess.REQUESTS_TIMEOUT_SECONDS)
        logger.debug('Ner service response: {}'.format(resp.status_code))

        if resp.status_code != constants.HTTP_SUCCESS_RESPONSE_CODE:
            raise NerAccessException(f'Could not upload corpus to NLP service. Status code: {resp.status_code}')

        logger.debug('Upload completed.')
        return resp.content.decode('utf-8')

    def _start_nlp_task(self, file_id):
        json_data = {
            'lpmn': f'filezip({file_id})|any2txt|wcrft2|liner2({{"model":"{self.ner_model}"}})|dir|makezip',
            'user': 'chronopressmap@outlook.com'
        }

        resp = requests.post(NerAccess.NLP_SERVICE_URL_BASE + "/startTask/", json=json_data,
                             headers={'Content-Type': 'application/json'},
                             timeout=NerAccess.REQUESTS_TIMEOUT_SECONDS)

        if resp.status_code != constants.HTTP_SUCCESS_RESPONSE_CODE:
            raise NerAccessException(f'NLP service start processing error. Status code: {resp.status_code}')

        return resp.content.decode('utf-8')

    def _poll_nlp_service_status(self, task_id):
        logger.debug('Polling nlp service status.')
        time.sleep(NerAccess.POLL_TIME_INTERVAL_SECONDS)
        data = self._get_status_req(task_id)

        while not self._is_nlp_task_finished(data):
            # if task is being processed, returned value is current progress indicator, it should be presented to
            # client, but this should be rather done in future
            time.sleep(NerAccess.POLL_TIME_INTERVAL_SECONDS)
            data = self._get_status_req(task_id)

        logger.debug('Service status: {}'.format(data['status']))

        if data['status'] == 'ERROR':
            raise NerAccessException('NLP service error: ' + data['value'])

        if len(data['value']) == 0:
            raise NerAccessException('Received improper response from NLP service')

        return data['value'][0]['fileID']

    def _get_status_req(self, task_id):
        resp = requests.get(NerAccess.NLP_SERVICE_URL_BASE + '/getStatus/' + task_id,
                            timeout=NerAccess.REQUESTS_TIMEOUT_SECONDS)

        if resp.status_code != constants.HTTP_SUCCESS_RESPONSE_CODE:
            raise NerAccessException(f'NLP service error. Status code: {resp.status_code}')

        return resp.json()

    def _is_nlp_task_finished(self, data):
        return data['status'] not in ('QUEUING', 'PROCESSING')

    def _store_ner_result(self, file_path, ner_resp_id):
        resp = requests.get(NerAccess.NLP_SERVICE_URL_BASE + '/download' + ner_resp_id,
                            timeout=NerAccess.REQUESTS_TIMEOUT_SECONDS)

        if resp.status_code != constants.HTTP_SUCCESS_RESPONSE_CODE:
            raise NerAccessException(f'Could not download results of NLP. Status code: {resp.status_code}')

        ner_result_path = constants.NER_RESULTS_FOLDER + os.path.basename(file_path) + '_ner_result.zip'
        with open(ner_result_path, "wb") as file_handle:
            file_handle.write(resp.content)

        return ner_result_path

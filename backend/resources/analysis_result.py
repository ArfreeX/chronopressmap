from flask import request
from flask_restful import Resource

import db


class AnalysisResult(Resource):
    def get(self):
        obj = db.Result.objects(uuid=request.args['uuid']).first()
        return obj.to_json(), 200

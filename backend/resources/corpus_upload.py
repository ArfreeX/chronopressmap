from flask_restful import Resource, reqparse
from .args_types import email_type, zipfile_type
from tasks import process_corpus
from utils.common import save_file_with_uuid
from dataclasses import asdict


class CorpusUpload(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('file',
                        type=zipfile_type,
                        location='files',
                        required=True)
    parser.add_argument('email', type=email_type, required=True)

    def post(self):
        data = self.parser.parse_args()
        file = data['file']
        corpus = save_file_with_uuid(file)
        corpus.email = data['email']
        process_corpus.delay(asdict(corpus))
        return {'message': 'File uploaded and processing started'}, 200

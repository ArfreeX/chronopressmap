#!/bin/bash

source ./venv/bin/activate

export LD_LIBRARY_PATH=/usr/local/lib/wrap_lem:$LD_LIBRARY_PATH
ldconfig

celery worker -A tasks -l info
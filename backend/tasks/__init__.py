from .celeryapp import celery
from .process_corpus import process_corpus


__all__ = ['celery', 'process_corpus']

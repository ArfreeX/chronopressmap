import os
import pathlib

from celery import Celery

from cache.redis_cache import RedisCache
from constants import (
    CELERY_BROKER_URL,
    CELERY_RESULT_BACKEND,
    NER_RESULTS_FOLDER,
    UPLOAD_FOLDER,
    REDIS_CACHE_URL
)
from box import init_box
from logger import logger, logger_init
from utils.common import chown
import db


def celery_init():
    logger_init('celery')
    logger.debug('Celery setup.')
    pathlib.Path(NER_RESULTS_FOLDER).mkdir(exist_ok=True)
    if os.getenv('DOCKER'):
        logger.debug('Running in docker environment.')
        chown(NER_RESULTS_FOLDER, 'celeryuser')
        chown(UPLOAD_FOLDER, 'celeryuser')
    cache_handler = RedisCache(url=REDIS_CACHE_URL)
    init_box(cache_handler=cache_handler)
    db.db_init()


def make_celery():
    celery = Celery(
        __name__,
        backend=CELERY_RESULT_BACKEND,
        broker=CELERY_BROKER_URL
    )
    celery_init()
    return celery


celery = make_celery()

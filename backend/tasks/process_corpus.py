import os

from core.corpus_file import Corpus
from core.metadata import load_metadata
import db
from nlp.nlp_chain import NlpChain
from tasks.celeryapp import celery
from logger import logger
from utils.zip_utils import prepare_corpus_zip


@celery.task
def process_corpus(corpus):
    corpus = Corpus(**corpus)
    corpus_zip = prepare_corpus_zip(corpus)
    if corpus_zip.status:
        corpus.meta = load_metadata(corpus_zip.meta)
        if corpus.meta is not None:
            nlp_chain = NlpChain()
            result = nlp_chain.run_nlp_processing_chain(corpus_zip.path)
            if result.success:
                result = make_result(result, corpus)
                logger.info('UUID: {}'.format(result.uuid))
                result.save()
                os.remove(corpus_zip.meta)
                os.remove(corpus_zip.path)


def make_result(result, corpus):
    corpus_files = []
    for corpus_filename, geocodes in result.text_geo_codes.items():
        corpus_file = db.CorpusFile(
            filename=corpus_filename,
            locations=[
                db.Location.from_geocode(geocode)
                for geocode in geocodes
            ],
            categories=corpus.meta[corpus_filename]
        )
        corpus_files.append(corpus_file)

    return db.Result(
        corpus_name=corpus.file,
        corpus_files=corpus_files,
        all_categories=corpus.meta.categories
    )

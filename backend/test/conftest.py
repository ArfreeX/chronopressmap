import os
import sys
import shutil
import pytest
import pathlib
import zipfile

import pandas as pd

sys.path.insert(0, str(pathlib.Path(__file__).absolute().parent.parent))  # noqa

import app
from constants import UPLOAD_FOLDER

TEST_BASE_DIR = str(pathlib.Path(__file__).absolute().parent)


@pytest.fixture
def client():
    _app = app.create_app()
    _app.config['TESTING'] = True
    with _app.test_client() as client:
        app.app_init()
        yield client
    remove_dir_contents(UPLOAD_FOLDER)


@pytest.fixture
def zipfile_payload(tmp_path):
    filename = str(tmp_path / 'test.zip')
    with zipfile.ZipFile(filename, mode='w') as zf:
        zf.writestr(filename, 'text')
    with open(filename, 'rb') as file:
        yield file, 'test.zip'


@pytest.fixture
def make_excel_path_from_csv(tmp_path):
    def make_excel(csv_path):
        filename = str(tmp_path / 'data.meta.xlsx')
        csv_meta_path = os.path.join(TEST_BASE_DIR, csv_path)
        pd.read_csv(csv_meta_path).to_excel(filename, index=False)
        return filename
    return make_excel


def remove_dir_contents(dir_path):
    for root, dirs, files in os.walk(dir_path):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))

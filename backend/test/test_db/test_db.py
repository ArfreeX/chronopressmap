import db


def test_start_empty_database():
    assert db.Result.objects().count() == 0


def test_db_can_save_result(result):
    result.save()
    assert db.Result.objects().count() == 1


def test_retrieve_result_by_uuid(result):
    result_uuid = result.uuid
    result.save()
    found_result = db.Result.objects(uuid=result_uuid).first()
    assert found_result.corpus_name == result.corpus_name


def test_can_save_result_with_count(result, location):
    corpus_file = result.corpus_files[0]
    location.count = 42
    corpus_file.locations.append(location)
    result.save()
    assert db.Result.objects().count() == 1

    saved_result = db.Result.objects(uuid=result.uuid).first()
    saved_location = saved_result.corpus_files[0].locations[0]
    assert saved_location.count == 42


def test_can_save_corpus_with_locations(result):
    corpus_file = result.corpus_files[0]
    locations = [
        db.Location(
            name='loc_1',
            count=2,
            center_lng=1.0,
            center_lat=2.0,
            is_center_preferred=True,
            geometry=db.LineString(
                geo_json={'type': 'LineString', 'coordinates': [[0.0, 0.0], [2.0, 4.0]]})
        )
    ]
    corpus_file.locations = locations
    result.save()
    assert db.Result.objects().count() == 1

    saved_result = db.Result.objects().first()
    saved_locations = saved_result.corpus_files[0].locations
    assert saved_locations == locations
    assert saved_locations[0].name == locations[0].name


def test_can_save_corpus_with_categories(result, dummy_categories):
    corpus_file = result.corpus_files[0]
    corpus_file.categories = dummy_categories
    result.save()
    assert db.Result.objects().count() == 1
    saved_result = db.Result.objects().first()
    saved_categories = saved_result.corpus_files[0].categories
    assert saved_categories == dummy_categories

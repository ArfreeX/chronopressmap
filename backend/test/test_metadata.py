import pandas as pd
import pytest

from core.metadata import MetaData, InvalidMetaError


def test_can_read_metadata_from_exel(make_excel_path_from_csv, mocker):
    valid_meta_excel_path = make_excel_path_from_csv('dummy_data/validmeta.csv')
    pd_read_excel_spy = mocker.spy(pd, 'read_excel')
    MetaData.from_excel(valid_meta_excel_path)
    pd_read_excel_spy.assert_called_with(valid_meta_excel_path)


@pytest.mark.parametrize('meta_path, is_valid', [
    ('dummy_data/validmeta.csv', True),
    ('dummy_data/invalidmeta.csv', False)
])
def test_can_validate_meta(make_excel_path_from_csv, meta_path, is_valid):
    meta_excel_path = make_excel_path_from_csv(meta_path)

    if is_valid:
        MetaData.from_excel(meta_excel_path)
    else:
        with pytest.raises(InvalidMetaError):
            MetaData.from_excel(meta_excel_path)


def test_can_retrieve_meta_for_single_file():
    data = {
        'filename': ['file_a', 'file_b'],
        'col_1': ['a', 'b'],
        'col_2': ['c', 'd'],
    }
    metadata = MetaData(pd.DataFrame(data))
    assert metadata['file_a'] == {
        'col_1': 'a',
        'col_2': 'c'
    }


def test_can_retrieve_categories_from_meta():
    data = {
        'filename': ['file_a', 'file_b', 'file_c', 'file_d'],
        'col_1': ['a', 'b', 'a', 'c'],
        'col_2': ['c', 'd', 'd', 'c'],
    }
    metadata = MetaData(pd.DataFrame(data))
    categories = metadata.categories
    expected_categories = {
        'col_1': ['a', 'b', 'c'],
        'col_2': ['c', 'd']
    }
    for name, values in categories.items():
        assert all(value in expected_categories[name] for value in values)

import os
import sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))


import pathlib

from box import init_box
from constants import NER_RESULTS_FOLDER, BASE_DIR
from nlp.nlp_chain import NlpChain


def init():
    cache_handler = None
    init_box(cache_handler)
    pathlib.Path(NER_RESULTS_FOLDER).mkdir(exist_ok=True)


if __name__ == '__main__':
    init()
    nlp_chain = NlpChain()
    result = nlp_chain.run_nlp_processing_chain(BASE_DIR + '/tmp/corpus.zip')
    print(result)

import uuid
import os
import pwd
import pathlib
from werkzeug.utils import secure_filename
from core.corpus_file import Corpus
from constants import UPLOAD_FOLDER
from logger import logger


def get_uuid():
    return str(uuid.uuid4()).replace('-', '')


def chown(dir_, username):
    userid = pwd.getpwnam(username).pw_uid
    os.chown(dir_, userid, userid)


def save_file_with_uuid(file):
    logger.debug('Saving file with UUID.')
    request_uuid = get_uuid()
    request_dir = os.path.join(UPLOAD_FOLDER, request_uuid)
    pathlib.Path(request_dir).mkdir()
    filepath = os.path.join(request_dir, secure_filename(file.filename))
    file.save(filepath)
    os.chmod(request_dir, 0o777)
    os.chmod(filepath, 0o777)
    logger.info('Saved file with UUID: {}'.format(request_uuid))
    return Corpus(request_uuid, request_dir, filepath, file.filename)

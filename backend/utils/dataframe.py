
def df_datetime_to_str(df):
    datetime_data = df.select_dtypes('datetime')
    datetime_columns = datetime_data.columns
    df[datetime_columns] = datetime_data.astype(str)

import zipfile
import shutil
import os
from constants import UPLOAD_FOLDER
from collections import namedtuple
from logger import logger

CorpusZipInfo = namedtuple('CorpusZipInfo', 'path meta status')


def validate_zip_format(zip_file):
    """
    :param zip_file: Path to zipfile or file-like object
    :return: True if file is valid zip file, false otherwise
    """
    logger.debug('Validating zipfile')
    return zipfile.is_zipfile(zip_file)


def iterate_zip_archive(path_to_zip):
    logger.debug('Iterating over zip archive')
    with zipfile.ZipFile(path_to_zip, 'r') as zip_file_handle:
        for archived_file_name in zip_file_handle.namelist():
            with zip_file_handle.open(archived_file_name, 'r') as archived_file:
                yield archived_file_name, archived_file


def get_meta_if_extracted(file_path):
    logger.debug('Trying to get meta')
    with zipfile.ZipFile(file_path, 'r') as handler:
        files = handler.namelist()
        metafiles = [meta.lower()
                     for meta in files if meta.lower().endswith('.meta.xlsx')]
        if len(metafiles) == 1:
            handler.extractall(os.path.dirname(file_path))
            logger.debug('Metafile found.')
            return metafiles[0]
        elif len(metafiles) > 1:
            handler.extractall(os.path.dirname(file_path))
            logger.warning(
                '[ZIP] Numerous files found %s. First is used.', metafiles)
            return metafiles[0]
        logger.error('Missing metafiles.')


def prepare_corpus_zip(corpus):
    logger.debug('Preparing corpus.zip')
    metadata_filename = get_meta_if_extracted(corpus.path)
    if metadata_filename is not None:
        os.remove(corpus.path)
        new_metadata_filename = os.path.join(UPLOAD_FOLDER,
                                             corpus.uuid+'-'+metadata_filename)
        metadata_filename = shutil.move(os.path.join(corpus.cwd, metadata_filename),
                                        new_metadata_filename)
        corpus_zip = shutil.make_archive(corpus.cwd, 'zip', corpus.cwd)
        shutil.rmtree(corpus.cwd)
        logger.debug('Preparation of corpus.zip done.')
        return CorpusZipInfo(corpus_zip, metadata_filename, True)
    os.remove(corpus.path)
    logger.warn('Missing metadata in file.')
    return CorpusZipInfo(None, 'No metadata in file', False)

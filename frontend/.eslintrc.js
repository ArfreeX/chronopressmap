var OFF = 0, WARN = 1, ERROR = 2;

module.exports = exports = {
    "env": {
        "es6": true
    },
    "parserOptions": {
        "sourceType": "module",
        "jsx": true
    },
    "extends": [
        "react-app"
    ],
    "rules": {
        "semi": [ ERROR, "always" ],
        "indent": [ WARN, 4 ],
        "quotes": [ WARN, "single" ],
        "jsx-quotes": [ WARN, "prefer-double" ]
    }
}

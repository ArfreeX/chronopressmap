import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
import NavBar from './components/Navbar';
import FileUploadView from './components/file-upload/FileUploadView';
import ResultView from './components/ResultView';
import { rootReducer }  from './reducers';

const store = createStore(rootReducer);

const App = () => {
    return (
        <Provider store={store}>
            <NavBar />
            <Router>
                <Switch>
                    <Route exact path="/" component={FileUploadView}/>
                    <Route path="/result/:uuid" component={ResultView}/>
                    <Route path="/result" component={Redirection}/>
                </Switch>
            </Router>
        </Provider>
    );
};

const Redirection = () => {
    return (
        <Redirect to="/" />
    );
};


export default App;

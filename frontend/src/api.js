import axios from 'axios';

export const api = {
    uploadFile: async (file, email, onUploadProgress) => {
        const formData = new FormData();
        formData.append('file', file);
        formData.append('email', email);
        return await axios.post('upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            onUploadProgress,
        });
    },
    fetchResult: async (uuid) => {
        return await axios.get('/result', {
            params: {
                'uuid': uuid
            }
        });
    }
};

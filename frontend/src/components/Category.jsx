import React from 'react';
import MultiSelect from 'react-multi-select-component';


const Category = props => {
    return (
        <div>
            <div style={{ marginBottom: 10 }}>
                {props.title}
            </div>
            <MultiSelect
                options={props.options}
                value={props.selected}
                onChange={props.onChange}
                labelledBy={'Select'}
            />
        </div>
    );
};

export default Category;
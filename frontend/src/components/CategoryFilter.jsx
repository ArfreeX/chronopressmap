import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Category from './Category';


const makeSelectOption = value => {
    return {
        value: value,
        label: value
    };
};

const categoriesToSelectOptions = categories => {
    const options = {};
    Object.keys(categories).forEach(key => {
        options[key] = categories[key].map(makeSelectOption);
    });
    return options;
};


const CategoryFilter = (props) =>  {
    const [selected, setSelected] = useState({});
    const options = categoriesToSelectOptions(props.categories);
    const onSelectChange = (category, selectedItems) => {
        setSelected({...selected, [category]: selectedItems});
    };
    useEffect(() => {
        props.filterCorpusFiles(selected);
    }, [selected, props]);

    return (
        <>
            {Object.keys(props.categories).map(key =>
                <div style={{ marginTop: 15 }} key={key}>
                    <Category
                        title={key}
                        selected={selected[key]}
                        options={options[key]}
                        onChange={(selectedItems) => onSelectChange(key, selectedItems)}
                    />
                </div>
            )}
        </>
    );
};

const mapDispatchToProps = dispatch => {
    return {
        filterCorpusFiles: (categoriesFilter) => dispatch({
            type: 'MARK_SELECTED_CORPUS_FILES',
            categoriesFilter: categoriesFilter
        })
    };
};

const mapStateToProps = state => {
    return {
        categories: state.categories,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryFilter);

import React from 'react';
import { connect } from 'react-redux';
import ListGroup from 'react-bootstrap/ListGroup';


const CorpusFiles = props => {
    const filesListItems = props.corpusFiles
        ? props.corpusFiles
            .map(file => {
                return (
                    <ListGroup.Item 
                        key={file.filename}
                        disabled={!file.selected}
                        variant={file.selected ? 'success' : 'light'}
                    >
                        {file.filename}
                    </ListGroup.Item>
                );
            })
        : null;
    return (
        <ListGroup variant="flush">
            {filesListItems}
        </ListGroup>
    );
};

const mapStateToProps = state => {
    return {
        corpusFiles: state.corpusFiles
    };
};

export default  connect(mapStateToProps, {})(CorpusFiles);

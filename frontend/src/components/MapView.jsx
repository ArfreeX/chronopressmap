import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GeoJSON, Map, Marker, Popup, TileLayer } from 'react-leaflet';
import { Sidebar, Tab } from 'react-leaflet-sidebarv2';
import CategoryFilter from './CategoryFilter';
import CorpusFiles from './CorpusFiles';


class MapView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mapCenter: [51.1078852, 17.0385376],
            zoom: 8,
            collapsed: false,
            selected: 'home'
        };
    }

    onClose() {
        this.setState({ collapsed: true });
    }

    onOpen(id) {
        this.setState({
            collapsed: false,
            selected: id
        });
    }

    placeLocation(location) {
        const default_popup =
            <Popup>
                <div>
                    <p>{location.name}, occurrences: {location.count}</p>
                </div>
            </Popup>;

        return location.is_center_preferred ?
            <Marker
                key={location.name}
                position={[location.center_lat, location.center_lng]}
            >
                {default_popup}
            </Marker>
            :
            <GeoJSON
                key={location.name}
                style={{fill: false}}
                data={location.geometry.geo_json}
            >
                {default_popup}
            </GeoJSON>;
    }

    render() {
        return (
            <div>
                <Sidebar
                    id="sidebar"
                    collapsed={this.state.collapsed}
                    selected={this.state.selected}
                    onOpen={this.onOpen.bind(this)}
                    onClose={this.onClose.bind(this)}
                    closeIcon="fa fa-caret-right"
                    position="right"
                >
                    <Tab id="home" header="Filters" icon="fa fa-filter">
                        <CategoryFilter/>
                    </Tab>
                    <Tab id="files" header="Filters" icon="fa fa-file-alt">
                        <CorpusFiles/>
                    </Tab>
                </Sidebar>
                <Map
                    center={this.state.mapCenter}
                    zoom={this.state.zoom}
                    style={{width: '100%', height: window.innerHeight}}
                    className="sidebar-map"
                >
                    <TileLayer
                        url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    />
                    {this.props.selectedLocations.map(location => this.placeLocation(location))}
                </Map>
            </div>
        );
    }
}


const mapStateProps = state => {
    return {
        selectedLocations: state.corpusFiles.filter(file => file.selected).map(file =>file.locations).flat()
    };
};

export default connect(mapStateProps, {})(MapView);

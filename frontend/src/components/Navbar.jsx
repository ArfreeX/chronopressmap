import React from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

const NavBar = props => {
    return (
        <Navbar
            className="navbar-color"
            collapseOnSelect
            expand="lg"
            variant="light"
            bg="light"
        >
            <Navbar.Brand href="/">chronopressmap</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav>
                    <Nav.Link href="/">Corpus Upload</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default NavBar;

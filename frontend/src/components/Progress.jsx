import React from 'react';
import PropTypes from 'prop-types';

const Progress = props => {
    const percentageStr = `${props.percentage}%`;
    return (
        <div className="progress">
            <div
                className="progress-bar progress-bar-striped bg-info"
                role="progressbar"
                style={{ width: percentageStr }}
            >
                {percentageStr}
            </div>
        </div>
    );
};

Progress.propTypes = {
    percentage: PropTypes.number.isRequired,
};

export default Progress;

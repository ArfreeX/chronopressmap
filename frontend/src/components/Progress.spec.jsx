import React from 'react';
import { render } from '@testing-library/react';
import Progress from './Progress';

it('should render without errors', () => {
    render(<Progress percentage={0} />);
});

import React, { Component } from 'react';
import { connect } from 'react-redux';
import MapView from './MapView';
import { api } from '../api';

class ResultView extends Component {
    constructor(props) {
        super(props);
        this.resultUUID = this.props.match.params.uuid;
        this.state = {
            '404': false
        };
    }

    async componentDidMount() {
        api.fetchResult(this.resultUUID)
            .then((response) => this.setData(JSON.parse(response.data)))
            .catch((error => this.setState({'404': true})));
    }

    setData(data) {
        this.props.setCategories(data.all_categories);
        this.props.setCorpusFiles(data.corpus_files);
    }

    render() {
        if (this.state['404']) {
            return (
                <div>Not found</div>
            );
        }

        return (
            <MapView />
        );
    }
}

const mapDispatchToProps = dispatch =>  {
    return {
        setCategories: (categories) => dispatch({type: 'SET_CATEGORIES', categories: categories}),
        setCorpusFiles: (corpusFiles) => dispatch({type: 'SET_CORPUS_FILES', corpusFiles: corpusFiles})
    };
};

export default connect(null, mapDispatchToProps)(ResultView);

import React, { Fragment, useState } from 'react';
import FileUploadForm from './FileUploadForm';
import Message from '../Message';
import Progress from '../Progress';
import { api } from '../../api';
import { updateUploadPercentage } from '../../utils';

const FileUpload = (props) => {
    const [file, setFile] = useState('');
    const [filename, setFilename] = useState('Choose File');
    const [message, setMessage] = useState('');
    const [uploadPercentage, setUploadPercentage] = useState(0);
    const [email, setEmail] = useState('');

    const onFileChange = e => {
        const choosedFile = e.target.files[0];
        if (choosedFile) {
            setFile(choosedFile);
            setFilename(choosedFile.name);
        }
    };

    const onEmailChange = e => {
        setEmail(e.target.value);
    };

    const resetUploader = () => {
        setFilename('Choose File');
        setFile('');
        setMessage('');
        setUploadPercentage(0);
    };

    const onSubmit = async e => {
        e.preventDefault();
        try {
            const response = await api.uploadFile(
                file, email, updateUploadPercentage(setUploadPercentage));
            setMessage(response.data.message);
            setTimeout(resetUploader, 5000);
        } catch (err) {
            if (!err.response) {
                setMessage('Unknown error');
                console.error(err);
            } else if (err.response.status === 500) {
                setMessage('Server error');
            } else {
                const errorData = err.response.data.message;
                let errorMessage = '';
                errorMessage += errorData.file ? errorData.file + '\n' : '';
                errorMessage += errorData.email ? errorData.file + '\n' : '';
                setMessage(errorMessage);
            }
        }
    };

    return (
        <Fragment>
            <FileUploadForm
                onFileChange={onFileChange}
                onEmailChange={onEmailChange}
                onSubmit={onSubmit}
                filename={filename}
            />
            <div style={{ marginTop: 10 }}>
                {uploadPercentage ? (
                    <Progress percentage={uploadPercentage} />
                ) : null}
            </div>
            <div style={{ marginTop: 10 }}>
                {message ? <Message msg={message} /> : null}
            </div>
        </Fragment>
    );
};

export default FileUpload;

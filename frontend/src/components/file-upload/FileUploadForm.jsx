import React from 'react';
import { Form } from 'react-bootstrap';

const FileUploadForm = props => {
    return (
        <Form onSubmit={props.onSubmit}>
            <Form.Label>Corpus zip</Form.Label>
            <div className="custom-file mb-4">
                <Form.Control
                    type="file"
                    className="custom-file-input"
                    id="customFile"
                    required
                    onChange={props.onFileChange}
                />
                <Form.Text className="text-muted">
                    Corpus file should be an zip archive
                </Form.Text>
                <Form.Label className="custom-file-label" htmlFor="customFile">
                    {props.filename}
                </Form.Label>
            </div>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type="email"
                    placeholder="Enter email"
                    required
                    onChange={props.onEmailChange}
                />
                <Form.Text className="text-muted">
                    We will send result link to this email.
                </Form.Text>
            </Form.Group>
            <Form.Control
                type="submit"
                value="Upload"
                className="btn btn-primary btn-block"
            />
        </Form>
    );
};

export default FileUploadForm;

import React from 'react';
import Table from 'react-bootstrap/Table';
import { Card } from 'react-bootstrap';

const ZipInfo = () => {
    return(
        <Card style={{ marginTop: 20 }}>
            <Card.Header>
              ZIP file structure
            </Card.Header>
            <Card.Body>
              Proper ZIP structure consists from:
                <ul>
                    <li>Files</li>
                    <li>Metadata file, which describes the each file</li>
                </ul>
                  Proper metadata file consist from:
                <ul>
                    <li>Filename ends with <b>.meta.xlsx</b></li>
                    <li>Excel spreadsheet cell <b>A1</b> contains key <b>filename</b></li>
                    <li>First row contains metadata keys, which will be used as a filters</li>
                    <li>First column contains filenames of files that are included in ZIP
                    and should be proceeded, except metadata filename</li>
                </ul>
                <Table striped bordered hover size="sm">
                    <tbody>
                        <tr>
                            <th>
                              Example.zip
                            </th>
                        </tr>
                        <tr>
                            <td>
                              filename1.txt
                            </td>
                        </tr>
                        <tr>
                            <td>
                              filename2.txt
                            </td>
                        </tr>
                        <tr>
                            <td>
                              example.meta.xlsx
                            </td>
                        </tr>
                    </tbody>
                </Table>
                <br></br>
                <b>Metadata file examaple:</b>
                <Table striped bordered hover size="sm">
                    <tbody>
                        <tr>
                            <td>
                                <b>filename</b>
                            </td>
                            <td>
                              author
                            </td>
                            <td>
                              publication date
                            </td>
                            <td>
                              etc.
                            </td>
                        </tr>
                        <tr>
                            <td>
                              filename1.txt
                            </td>
                            <td>
                              Oskar T.
                            </td>
                            <td>
                              2019-02-08
                            </td>
                        </tr>
                        <tr>
                            <td>
                              filename2.txt
                            </td>
                            <td>
                              Jadwiga S.
                            </td>
                            <td>
                              1989-02-08
                            </td>
                        </tr>
                    </tbody>
                </Table>
            </Card.Body>
        </Card>
    );
};

export default ZipInfo;

import { combineReducers } from 'redux';


export const categories = (state = {}, action) => {
    if (action.type === 'SET_CATEGORIES'){
        return action.categories;
    } else {
        return state;
    }
};


export const corpusFiles  = (state = [], action) => {
    switch(action.type){
    case 'SET_CORPUS_FILES':
        return action.corpusFiles;
    case 'MARK_SELECTED_CORPUS_FILES':
        return markSelectedCorpusFiles(state, action.categoriesFilter);
    default:
        return state;
    }
};


const markSelectedCorpusFiles = (files, categoriesFilter) => {
    return files.map(
        (file) => {
            const selected = Object.keys(categoriesFilter).some(
                (category) => {
                    const categoryValues = categoriesFilter[category].map(item => item.value);
                    const fileCategory = file.categories[category];
                    // eslint-disable-next-line
                    return fileCategory ? categoryValues.some(el => el == fileCategory) : false;
                }
            );
            return {
                ...file,
                selected
            };
        }
    );
};


export const rootReducer = combineReducers({
    categories,
    corpusFiles
});

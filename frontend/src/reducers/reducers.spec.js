import { corpusFiles } from './index';

describe('corpusFiles reducer', () => {
    it('should return the initial state', () => {
        expect(corpusFiles(undefined, {})).toEqual([]);
    });
  
    it('should set given corpus files', () => {
        const exampleFiles = [
            {
                fileanme: 'test'
            }
        ];
        expect(corpusFiles([], {type: 'SET_CORPUS_FILES', corpusFiles:exampleFiles }))
            .toEqual(exampleFiles);
    });

    it('should mark selected corpus files', () => {
        const exampleFiles = [
            {
                fileanme: 'test_1',
                categories: {
                    kind: 'sport',
                    year:  1988
                }
            },
            {
                fileanme: 'test_2',
                categories: {
                    kind: 'sport'
                }
            }
        ];
        const categoriesFilter = {year:[{value:1988,label:1988}]};
        const expectedOutputFiles = [
            {
                selected: true,
                fileanme: 'test_1',
                categories: {
                    kind: 'sport',
                    year: 1988,
                }
            },
            {
                selected: false,
                fileanme: 'test_2',
                categories: {
                    kind: 'sport'
                }
            }
        ];
        expect(
            corpusFiles(
                exampleFiles, 
                {
                    type: 'MARK_SELECTED_CORPUS_FILES', 
                    categoriesFilter: categoriesFilter
                })
        ).toEqual(expectedOutputFiles);
    });

    it('should deselected selected', () => {
        const exampleFiles = [
            {
                selected: true,
                fileanme: 'test_1',
                categories: {
                    kind: 'sport',
                    year: 1988,
                }
            },
            {
                selected: true,
                fileanme: 'test_2',
                categories: {
                    kind: 'sport',
                }
            }
        ];
        const categoriesFilter = {};
        const expectedOutputFiles = [
            {
                selected: false,
                fileanme: 'test_1',
                categories: {
                    kind: 'sport',
                    year: 1988,
                }
            },
            {
                selected: false,
                fileanme: 'test_2',
                categories: {
                    kind: 'sport',
                }
            }
        ];
        expect(
            corpusFiles(
                exampleFiles, 
                {
                    type: 'MARK_SELECTED_CORPUS_FILES', 
                    categoriesFilter: categoriesFilter
                })
        ).toEqual(expectedOutputFiles);
    });

    it('should handle different data types', () => {
        const exampleFiles = [
            {
                categories: {
                    year:  1988
                }
            },
        ];
        const categoriesFilter = {year:[{value:'1988',label: '1988'}]};
        const expectedOutputFiles = [
            {
                selected: true,
                categories: {
                    year: 1988,
                }
            }
        ];
        expect(
            corpusFiles(
                exampleFiles, 
                {
                    type: 'MARK_SELECTED_CORPUS_FILES', 
                    categoriesFilter: categoriesFilter
                })
        ).toEqual(expectedOutputFiles);
    });

});